from pyglet.gl import *
import time
import numpy as np
import copy
import random


class camera:
    def __init__(self):
        self.pos = [0, 0, 0]
        self.look = [0, 0, 0]

    def set_pos(self, pos):
        self.pos = pos

    def set_look(self, look):
        self.look = look


class cestica:
    def __init__(self, pos, brzina, boja, velicina, evolve=False, randomdir=False):
        self.pos = pos
        self.brzina = brzina
        self.cboja = boja[0]
        self.dboja = boja[1]
        self.cvelicina = velicina[0]
        self.dvelicina = velicina[1]
        self.zivot = time.time()
        self.evolve = evolve
        self.randomdir = randomdir

    def unikat(self):
        self.zivot = time.time() + abs(random.gauss(0.0, 15))

    def update(self, t):
        if self.randomdir:
            operators = ["+","-"]
            for i in range(3):
                picked_operator = random.choice(operators)
                if picked_operator == "+":
                    self.pos[i] = self.pos[i] + self.brzina[i] * t
                if picked_operator == "-":
                    self.pos[i] = self.pos[i] - self.brzina[i] * t
        else:
            self.pos[0] -= self.brzina[0] * t
            self.pos[1] -= self.brzina[1] * t
            self.pos[2] -= self.brzina[2] * t

        if self.evolve:
            bx = self.cboja[0] + (self.dboja[0] - self.cboja[0]) * 1/10000
            by = self.cboja[1] + (self.dboja[1] - self.cboja[1]) * 1/10000
            bz = self.cboja[2] + (self.dboja[2] - self.cboja[2]) * 1/10000
            self.cboja = [bx, by, bz]

            vx = self.cvelicina[0] + (self.dvelicina[0] - self.cvelicina[0]) * 1/1000
            vy = self.cvelicina[1] + (self.dvelicina[1] - self.cvelicina[1]) * 1/1000
            self.cvelicina = [vx, vy]


class skupcestica:
    def __init__(self, cestica, brcestica, textura):
        self.cestica = cestica
        self.skup = []
        self.textura = textura

        for i in range(brcestica):
            self.dodajcesticu()

    def update(self, t):
        for cestica in self.skup:
            cestica.update(t)
        et = time.time()
        for i in range(len(self.skup)):
            if self.skup[i].zivot <= et:
                del self.skup[i]
                self.dodajcesticu()

    def dodajcesticu(self):
        instanca = copy.deepcopy(self.cestica)
        instanca.unikat()
        self.skup.append(instanca)

    def draw(self):
        glEnable(self.textura.target)
        glBindTexture(self.textura.target, self.textura.id)
        glEnable(GL_BLEND)
        glBlendFunc(GL_ONE, GL_ONE)
        glPushMatrix()

        for cestica in self.skup:
            glColor3f(cestica.cboja[0], cestica.cboja[1], cestica.cboja[2])

            matrix = (GLfloat * 16)()
            glGetFloatv(GL_MODELVIEW_MATRIX, matrix)
            matrix = list(matrix)
            CameraRight = np.array([matrix[2], matrix[6], matrix[10]])
            CameraUp = np.array([0, 1, 0])
            cvel = cestica.cvelicina
            CameraRight = np.cross(CameraRight, CameraUp)
            v1 = cestica.pos + CameraRight * 0.5 * cvel[0] + CameraUp * -0.5 * cvel[1]
            v2 = cestica.pos + CameraRight * 0.5 * cvel[0] + CameraUp * 0.5 * cvel[1]
            v3 = cestica.pos + CameraRight * -0.5 * cvel[0] + CameraUp * -0.5 * cvel[1]
            v4 = cestica.pos + CameraRight * -0.5 * cvel[0] + CameraUp * 0.5 * cvel[1]
            glBegin(GL_QUADS)
            glTexCoord2f(0, 0)
            glVertex3f(v3[0], v3[1], v3[2])
            glTexCoord2f(1, 0)
            glVertex3f(v4[0], v4[1], v4[2])
            glTexCoord2f(1, 1)
            glVertex3f(v2[0], v2[1], v2[2])
            glTexCoord2f(0, 1)
            glVertex3f(v1[0], v1[1], v1[2])
            glEnd()

        glDisable(GL_BLEND)
        glPopMatrix()
        glDisable(self.textura.target)


class window(pyglet.window.Window):
    def __init__(self):
        super().__init__(width=1280, height=960)
        self.camera = camera()
        self.camera.set_pos([0, 0, 1000])
        pyglet.clock.schedule_interval(self.update, 1/60)

        smoketexture = pyglet.image.load('smoke.bmp').get_texture()
        smokecestica = cestica([0, 5, 0], [5, -5, 0], [[0.1, 0.1, 0.1], [1, 4, 2]], [[200, 200], [2000, 2000]], evolve=True)
        self.skupdimacestica = skupcestica(smokecestica, 20, smoketexture)

        beetexture = pyglet.image.load('Bee.jpg').get_texture()
        beecestica = cestica([0, 5, 0], [500, 500, 500], [[1, 1, 1], [1, 4, 2]], [[200, 200], [2000, 2000]], randomdir=True)
        self.skuppcelacestica = skupcestica(beecestica, 4, beetexture)

    def update(self, t):
        self.skupdimacestica.update(t)
        self.skuppcelacestica.update(t)

    def on_draw(self):
        self.clear()
        glClear(GL_COLOR_BUFFER_BIT)

        glMatrixMode(GL_PROJECTION)
        glLoadIdentity()
        gluPerspective(60, 1280 / 960, 0.05, 10000)
        glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()

        gluLookAt(self.camera.pos[0], self.camera.pos[1], self.camera.pos[2],
                  self.camera.look[0], self.camera.look[1], self.camera.look[2],
                  0.0, 1.0, 0.0)

        glPushMatrix()
        self.skupdimacestica.draw()
        self.skuppcelacestica.draw()
        glPopMatrix()
        glFlush()


window = window()
pyglet.app.run()