using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuSceneControler : MonoBehaviour
{
    public void GameScene()
    {
        SceneManager.LoadScene("GameScene");
    }

    public void HeroSelectScene()
    {
        SceneManager.LoadScene("HeroSelectScene");
    }

    public void MainMenuScene()
    {
        SceneManager.LoadScene("MenuScene");
    }
}
