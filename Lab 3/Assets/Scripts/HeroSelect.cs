using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HeroSelect : MonoBehaviour
{
    [SerializeField] GameObject DisplayModel;
    [SerializeField] List<Sprite> Models = new List<Sprite>();
    [SerializeField] GameObject NameGO;
    List<string> Names = new List<string>();
    public SpriteRenderer spriteRenderer;
    public Text Name;
    public static int current = 0;

    void Start()
    {
        Names.Add("Arok");
        Names.Add("Leya");
        Names.Add("Bral");
        spriteRenderer = DisplayModel.GetComponent<SpriteRenderer>();
        Name = NameGO.GetComponent<Text>();

        spriteRenderer.sprite = Models[current];
        Name.text = Names[current];
        PositionModel();
    }

    public void Next()
    {
        if(current == 2)
        {
            current = 0;
        }
        else
        {
            current++;
        }

        spriteRenderer.sprite = Models[current];
        Name.text = Names[current];

        PositionModel();
    }
    public void Previous()
    {
        if (current == 0)
        {
            current = 2;
        }
        else
        {
            current--;
        }
        spriteRenderer.sprite = Models[current];
        Name.text = Names[current];

        PositionModel();
    }

    public void Confirm()
    {
        //zapamti heroja
        SceneManager.LoadScene("MenuScene");
    }

    public void PositionModel()
    {
        if (current == 0)
        {
            DisplayModel.transform.localScale = new Vector3(2f,2f,2f);
            DisplayModel.transform.position = new Vector3(0, -3, 0);
        }
        if (current == 1)
        {
            DisplayModel.transform.localScale = new Vector3(7, 7, 2);
            DisplayModel.transform.position = new Vector3(0.5f, -1.5f, 0);
        }
        if (current == 2)
        {
            DisplayModel.transform.localScale = new Vector3(6, 6, 2);
            DisplayModel.transform.position = new Vector3(-0.2f, 0, 0);
        }
    }

}
