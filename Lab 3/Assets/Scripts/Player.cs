using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Player : MonoBehaviour
{
    [SerializeField] public List <GameObject> MaxHP = new List<GameObject>();
    [SerializeField] public int chargetime;
    [SerializeField] List<Sprite> Models = new List<Sprite>();
    [SerializeField] public GameObject EnergiField;
    [SerializeField] public float MaxEnergy;
    [SerializeField] public GameObject effect;

    public int hp;
    public Slider slider;
    public float energy = 0;
    public SpriteRenderer spriteRenderer;
    public static int level = 1;

    void Start()
    {
        Debug.Log(level);
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();
        spriteRenderer.sprite = Models[HeroSelect.current];
        PositionModel();

        slider = EnergiField.GetComponent<Slider>();

        hp = 3;
    }

    void Update()
    {
        bool v = Input.GetKey(KeyCode.Space);
        if (v)
        {
            effect.SetActive(true);
            energy += 1;
            slider.value = energy/MaxEnergy;

        }
        else
        {
            effect.SetActive(false);
        }

        if (spriteRenderer.flipX == false)
        {
            if (HeroSelect.current == 0)
            {
                effect.transform.localPosition = new Vector3(-0.1f, 0.6f, 0);
            }
            if (HeroSelect.current == 1)
            {
                effect.transform.localPosition = new Vector3(-0.07f, -0.05f, 0);
            }
            if (HeroSelect.current == 2)
            {
                effect.transform.localPosition = new Vector3(0, -0.25f, 0);
            }
        }
        else if (spriteRenderer.flipX == true)
        {
            if (HeroSelect.current == 0)
            {
                effect.transform.localPosition = new Vector3(0.1f, 0.6f, 0);
            }
            if (HeroSelect.current == 1)
            {
                effect.transform.localPosition = new Vector3(0.07f, -0.05f, 0);
            }
            if (HeroSelect.current == 2)
            {
                effect.transform.localPosition = new Vector3(0, -0.25f, 0);
            }
        }

        if (energy == MaxEnergy)
        {
            level += 1;
            Debug.Log(level);
            energy = 0;
        }

        if (hp <= 0)
        {
            level = 1;
            SceneManager.LoadScene("GameOverScene");
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        Destroy(MaxHP[hp-1]);
        hp -= 1;
    }

    public void PositionModel()
    {
        if (HeroSelect.current == 0)
        {
            gameObject.transform.localScale = new Vector3(1f, 1f, 1f);
            effect.transform.localScale = new Vector3(2f, 2f, 1);
            effect.transform.localPosition = new Vector3(-0.1f, 0.6f, 0);
        }
        if (HeroSelect.current == 1)
        {
            gameObject.transform.localScale = new Vector3(5f, 5f, 1f);
            effect.transform.localScale = new Vector3(0.5f, 0.5f, 1);
            effect.transform.localPosition = new Vector3(-0.07f, -0.05f, 0);
        }
        if (HeroSelect.current == 2)
        {
            gameObject.transform.localScale = new Vector3(3f, 3f, 1f);
            effect.transform.localScale = new Vector3(0.7f, 0.7f, 1);
            effect.transform.localPosition = new Vector3(0, -0.25f, 0);
        }
    }
}
