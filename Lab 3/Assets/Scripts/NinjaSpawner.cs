using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NinjaSpawner : MonoBehaviour
{
    [SerializeField] public float lifetime;
    [SerializeField] public GameObject ninjaThrow;
    [SerializeField] public float shoottimer;
    [SerializeField] public float flySpeed;

    public GameObject spawns;
    public GameObject helpGO;
    List<GameObject> projectilelist = new List<GameObject>();
    public SpriteRenderer spriteRenderer;
    public Vector2 direction;
    public float helpshot = 0;
    private List<float> angleList = new List<float>();
    private Animator anim;

    void Start()
    {
        spawns = GameObject.Find("Spawns");
        anim = GetComponent<Animator>();

        angleList.Add(0);
        angleList.Add(1);
        angleList.Add(-1);

        helpshot = shoottimer;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (gameObject.transform.position.x > 0)
        {
            spriteRenderer.flipX = true;
        }

        if (gameObject.transform.position.x == 9)
        {
            direction.x = -1;

        }
        if (gameObject.transform.position.x == -9)
        {
            direction.x = 1;
            
        }
        if (gameObject.transform.position.y == 4)
        {
            direction.y = -1;
        }
    }

    void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;
            shoottimer -= Time.deltaTime;
        }
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
        if (shoottimer < 0)
        {
            anim.SetTrigger("Shoot");
            Shoot();
            shoottimer = helpshot;
        }

        int i = 0;
        foreach (GameObject projectile in projectilelist)
        {
            Vector2 angleDir = new Vector2();

            if (gameObject.transform.position.x == 9)
            {
                angleDir.x = direction.x;
                angleDir.y = angleList[i];

            }
            if (gameObject.transform.position.x == -9)
            {
                angleDir.x = direction.x;
                angleDir.y = angleList[i];

            }
            if (gameObject.transform.position.y == 4)
            {
                angleDir.x = angleList[i];
                angleDir.y = direction.y;
            }
            
            projectilelist[i].transform.Translate(angleDir * flySpeed * Time.deltaTime);
            i++;
        }
    }

    public void Shoot()
    { 

        for(int i = 0; i < 3; i++)
        {
            helpGO = Instantiate(ninjaThrow, gameObject.transform.position, Quaternion.identity, spawns.transform);
            projectilelist.Add(helpGO);

            SpriteRenderer helpsprite = helpGO.GetComponent<SpriteRenderer>();

            if (gameObject.transform.position.x < 0)
            {
                helpsprite.flipX = true;
            }
        }
        
    }
}
