using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    [SerializeField] public float lifetime;
    [SerializeField] public GameObject fireballSeq;
    [SerializeField] public float shoottimer;
    [SerializeField] public float flySpeed;

    public GameObject spawns;
    public GameObject helpGO;
    List<GameObject> projectilelist = new List<GameObject>();
    public SpriteRenderer spriteRenderer;
    public Vector2 direction;
    public float helpshot = 0;
    void Start()
    {
        spawns = GameObject.Find("Spawns");

        helpshot = shoottimer;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (gameObject.transform.position.x > 0)
        {
            spriteRenderer.flipX = true;
        }

        if (gameObject.transform.position.x == 9)
        {
            direction.x = 1;

            if (gameObject.transform.position.y >= 0)
            {
                direction.y = Random.Range(-1, 0);
            }
            else
            {
                direction.y = Random.Range(0, 1);
            }
        }
        if (gameObject.transform.position.x == -9)
        {
            direction.x = 1;
            
            if (gameObject.transform.position.y >= 0)
            {
                direction.y = Random.Range(-1, 0);
            }
            else
            {
                direction.y = Random.Range(0, 1);
            }
        }
        if (gameObject.transform.position.y == 4)
        {
            direction.y = -1;

            if (gameObject.transform.position.x >= 0)
            {
                direction.x = Random.Range(-1, 0);
            }
            else
            {
                direction.x = Random.Range(0, 1);
            }

        }
    }

    void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;
            shoottimer -= Time.deltaTime;
        }
        if (lifetime < 0)
        {
            Destroy(gameObject);
            foreach(GameObject fireball in projectilelist)
            {
                Destroy(fireball);
            }
        }
        if (shoottimer < 0)
        {
            Shoot();
            shoottimer = helpshot;
        }
        foreach (GameObject go in projectilelist)
        {
            go.transform.Translate(direction * flySpeed * Time.deltaTime);
        }
    }

    public void Shoot()
    {
        helpGO = Instantiate(fireballSeq, gameObject.transform.position, Quaternion.identity, spawns.transform);
        projectilelist.Add(helpGO);


        if (gameObject.transform.position.x > 0)
        {
            var rot = helpGO.transform.rotation;
            rot.z += 180f;
            helpGO.transform.rotation = rot;
        }
    }
}
