using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShootingScript : MonoBehaviour
{
    [SerializeField] public float time;
    [SerializeField] public float spawntime;
    [SerializeField] public List<Sprite> Backgrounds = new List<Sprite>();
    [SerializeField] public GameObject background;
    [SerializeField] public GameObject spawns;
    [SerializeField] public List<GameObject> Spawners = new List<GameObject>();
    [SerializeField] public AudioClip audioClip;
    [SerializeField] public AudioClip spawnClip;

    public float spawnhelptimer = 0;
    public List<string> SpawnSide = new List<string>();
    public int currentLevel = Player.level;
    public int testLevel = 0;
    public SpriteRenderer spriteRenderer;

    void Start()
    {
        SpawnSide.Add("up");
        SpawnSide.Add("right");
        SpawnSide.Add("left");
        spawnhelptimer = spawntime;
        spriteRenderer = background.GetComponent<SpriteRenderer>();
        CreateSpawners();
    }


    public void CreateSpawners()
    {
        AudioSource.PlayClipAtPoint(spawnClip, transform.position);

        for (int i=0; i < currentLevel+2; i++)
        {
            string side = "";
            GameObject spawner;
            Vector2 SpawnPosition = new Vector2();

            int rSide = Random.Range(0, 3);
            int rSpawner = Random.Range(0, currentLevel);
            
            spawner = Spawners[rSpawner];
            side = SpawnSide[rSide];
            while(spawner != Spawners[0] && side == "up")
            {
                rSide = Random.Range(0, 3);
                side = SpawnSide[rSide];
            }
          

            if(side == "up")
            {
                SpawnPosition.x = Random.Range(-8.5f, 8.5f); ;
                SpawnPosition.y = 4;
                
            }
            if (side == "left")
            {
                SpawnPosition.y = Random.Range(-4f, 4f); ;
                SpawnPosition.x = -9f;
            }
            if (side == "right")
            {
                SpawnPosition.y = Random.Range(-4f, 4f); ;
                SpawnPosition.x = 9f;
            }

            Instantiate(spawner, SpawnPosition, Quaternion.identity, spawns.transform);
        }
    }

    void Update()
    {
        testLevel = Player.level;
        if (testLevel != currentLevel)
        {
            ChangeLevel();
        }
        if (time > 0)
        {
            time -= Time.deltaTime;
            spawntime -= Time.deltaTime;
        }

        if (spawntime < 0)
        {
            CreateSpawners();
            spawntime = spawnhelptimer;
        }
    }

    void ChangeLevel()
    {
        currentLevel = Player.level;
        if (currentLevel == 4)
        {
            Player.level = 1;
            SceneManager.LoadScene("VictoryScene");
        }
        else
        {
            AudioSource.PlayClipAtPoint(audioClip, transform.position);
            spriteRenderer.sprite = Backgrounds[currentLevel - 1];
            if (currentLevel != 1)
            {
                DestroyAllSpawns();
                CreateSpawners();
                spawntime = spawnhelptimer;
            }
        }
        
        
    }

    public void DestroyAllSpawns()
    {
        foreach (Transform child in spawns.transform)
        {
            Destroy(child.gameObject);
        }
    }
}
