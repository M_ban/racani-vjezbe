using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] public GameObject player;
    [SerializeField] private float flySpeed;
    public SpriteRenderer spriteRenderer;

    void Start()
    {
        spriteRenderer = player.GetComponent<SpriteRenderer>();
    }


    void Update()
    {
        float v = Input.GetAxisRaw("Vertical");
        float h = Input.GetAxisRaw("Horizontal");
        bool space = Input.GetKey(KeyCode.Space);
        Vector2 direction = new Vector2(h, v);

        if (h > 0)
        {
            spriteRenderer.flipX = false;
        }
        else if (h < 0)
        {
            spriteRenderer.flipX = true;
        }

        Vector2 currpos = player.transform.position;
        if (!space)
        {
            player.transform.Translate(direction * flySpeed * Time.deltaTime);
        }
        
        
        if (currpos.x > 8.5 && h > 0)
        {
            player.transform.position = currpos;
        }
        if (currpos.x < -8.5 && h < 0)
        {
            player.transform.position = currpos;
        }
        if (currpos.y > 3 && v > 0)
        {
            player.transform.position = currpos;
        }
        if (currpos.y < -3 && v < 0)
        {
            player.transform.position = currpos;
        }
    }

}
