using UnityEngine;

public class DragonSpawner : MonoBehaviour
{
    [SerializeField] public float lifetime;
    [SerializeField] public GameObject DragonBall;
    [SerializeField] public float shoottimer;
    [SerializeField] public float flySpeed;

    public GameObject spawns;
    public GameObject helpGO;
    public SpriteRenderer spriteRenderer;
    public Vector2 velocity;
    public float helpshot = 0;
    public Rigidbody2D rb;
    private Animator anim;

    void Start()
    {
        spawns = GameObject.Find("Spawns");
        anim = GetComponent<Animator>();

        helpshot = shoottimer;
        spriteRenderer = gameObject.GetComponent<SpriteRenderer>();

        if (gameObject.transform.position.x > 0)
        {
            spriteRenderer.flipX = true;
        }

        if (gameObject.transform.position.x == 9)
        {
            velocity.x = Random.Range(-10, -5);
            velocity.y = Random.Range(5, 10);

        }
        if (gameObject.transform.position.x == -9)
        {
            velocity.x = Random.Range(5, 10); ;
            velocity.y = Random.Range(5, 10); ;

        }
    }

    void Update()
    {
        if (lifetime > 0)
        {
            lifetime -= Time.deltaTime;
            shoottimer -= Time.deltaTime;
        }
        if (lifetime < 0)
        {
            Destroy(gameObject);
        }
        if (shoottimer < 0)
        {
            anim.SetTrigger("Shoot");
            Shoot();
            shoottimer = helpshot;
        }

    }

    public void Shoot()
    {
        helpGO = Instantiate(DragonBall, gameObject.transform.position, Quaternion.identity, spawns.transform);
        rb = helpGO.GetComponent<Rigidbody2D>();
        rb.velocity = velocity;
        SpriteRenderer helpsprite = helpGO.GetComponent<SpriteRenderer>();
    }
}
