from pyglet.gl import *
import numpy as np
from numpy.linalg import norm
import time


class camera:
    def __init__(self):
        self.pos = [0, 0, 0]
        self.look = [0, 0, 0]

    def set_pos(self, pos):
        self.pos = pos

    def set_look(self, look):
        self.look = look


class bspline:
    def __init__(self):
        self.tocke = []
        self.ucitaj("bspline.obj")
        self.skaliranje()

    def ucitaj(self, file):
        ofile = open(file, 'r')

        for linija in ofile:
            if linija.startswith("v"):
                dots = linija.split()
                self.tocke.append(list(map(float, dots[1:4])))
            else:
                continue

    def izracunaj_orijentaciju(self, i, t):
        T = np.array([t ** 3, t ** 2, t, 1])

        B = np.array([[-1, 3, -3, 1], [3, -6, 3, 0], [-3, 0, 3, 0], [1, 4, 1, 0]])
        B = (1 / 6) * B

        R = np.array([self.tocke[i - 1],
                      self.tocke[i],
                      self.tocke[i + 1],
                      self.tocke[i + 2]])
        TB = np.dot(T, B)
        orijentacija = np.dot(TB, R)
        return orijentacija

    def izracunaj_tangentu(self, i, t):
        T = [t ** 2, t, 1]
        B = np.array([[-1, 3, -3, 1], [2, -4, 2, 0], [-1, 0, 1, 0]])
        B = 1 / 2 * B

        R = np.array([self.tocke[i - 1],
                      self.tocke[i],
                      self.tocke[i + 1],
                      self.tocke[i + 2]])
        TB = np.dot(T, B)
        tangenta = np.dot(TB, R)
        return tangenta

    def skaliranje(self):
        x_max, x_min = float("-inf"), float("inf")
        y_max, y_min = float("-inf"), float("inf")
        z_max, z_min = float("-inf"), float("inf")

        for v in self.tocke:
            if v[0] < x_min:
                x_min = v[0]
            if v[0] > x_max:
                x_max = v[0]

            if v[1] < y_min:
                y_min = v[1]
            if v[1] > y_max:
                y_max = v[1]

            if v[2] < z_min:
                z_min = v[2]
            if v[2] > z_max:
                z_max = v[2]

        scale_x = x_max - x_min
        scale_y = y_max - y_min
        scale_z = z_max - z_min

        self.scale = max([scale_x, scale_y, scale_z])


class object:
    def __init__(self):
        self.tocke = []
        self.poly = []
        self.ucitaj("tetrahedron.obj")

    def ucitaj(self, file):
        ofile = open(file, 'r')

        for linija in ofile:
            if linija.startswith("v"):
                dots = linija.split()
                self.tocke.append(list(map(float, dots[1:4])))
            elif linija.startswith("f"):
                pols = linija.split()
                self.poly.append(list(map(float, pols[1:4])))
            else:
                continue

    def izracunaj_os(self, trenutni, ciljni):
        os = np.cross(trenutni, ciljni)
        return os

    def izracunaj_kut(self, trenutni, ciljni):
        se = np.dot(trenutni, ciljni)
        s = np.linalg.norm(trenutni)
        e = np.linalg.norm(ciljni)
        kut = se / (s * e)
        kut = np.arccos(kut)
        kut = np.rad2deg(kut)
        return kut

    def draw_setup(self):
        self.batch = pyglet.graphics.Batch()
        for pol in self.poly:
            V1 = self.tocke[int(pol[0]) - 1]
            V2 = self.tocke[int(pol[1]) - 1]
            V3 = self.tocke[int(pol[2]) - 1]
            self.batch.add(3, GL_TRIANGLES, None, ('v3f', (V1[0] / 15, V1[1] / 15, V1[2] / 15, V2[0] / 15, V2[1] / 15, V2[2] / 15, V3[0] / 15, V3[1] / 15, V3[2] / 15)), ('c3d', (0, 0.6, 0, 0, 0.6, 0, 0, 0.6, 0)))

    def draw(self):
        self.batch.draw()


class Window(pyglet.window.Window):
    def __init__(self, w, h, ime, cilj_ori, tangente, rot_kut, rot_osi):
        self.window = pyglet.window.Window(w, h)
        self.window.set_caption(ime)

        self.cilj_ori = cilj_ori
        self.tangente = tangente
        self.rot_kut = rot_kut
        self.rot_osi = rot_osi

        self.batch = pyglet.graphics.Batch()
        self.obj = object()
        self.spline = bspline()
        self.camera = camera()
        self.on_draw()

    def on_draw(self):
        self.camera.set_pos([0, 0, 0])
        self.camera.set_look([6, 2, 10])
        gluLookAt(self.camera.pos[0], self.camera.pos[1], self.camera.pos[2],
                  self.camera.look[0], self.camera.look[1], self.camera.look[2],
                  0.0, 1.0, 0.0)
        self.draw_bspline()
        self.draw_object()
        self.cnt = 0

        #while 1:
        for i in range(8):
            self.clear()
            self.draw_bspline()
            self.move_object()
            time.sleep(3)
            self.cnt += 20

    def draw_bspline(self):
        line_dots = []
        line_boja = []
        for dot in self.cilj_ori:
            dot_help = []
            line_dots.append(dot[0] / self.spline.scale)
            line_dots.append(dot[1] / self.spline.scale)
            line_dots.append(dot[2] / self.spline.scale)
            line_boja.append(0.6)
            line_boja.append(0.6)
            line_boja.append(0.6)
        self.batch.add(int(line_dots.__len__() / 3), GL_LINE_STRIP, None, ('v3f', line_dots), ('c3d', line_boja))
        # print(len(cilj_ori))
        glPushMatrix()
        self.batch.draw()
        glPopMatrix()

    def draw_object(self):
        # for pol in self.obj.poly:
        # V1 = self.obj.tocke[int(pol[0]) - 1]
        # V2 = self.obj.tocke[int(pol[1]) - 1]
        # V3 = self.obj.tocke[int(pol[2]) - 1]
        # self.batch.add(3, GL_TRIANGLES, None, ('v3f', (V1[0]/10, V1[1]/10, V1[2]/10, V2[0]/10, V2[1]/10, V2[2]/10, V3[0]/10, V3[1]/10, V3[2]/10)), ('c3d', (0, 0.6, 0, 0, 0.6, 0, 0, 0.6, 0)))
        self.obj.draw_setup()
        glPushMatrix()
        # self.batch.draw()
        #self.obj.draw()
        glPopMatrix()

    def move_object(self):
        #while self.cnt < 180:
            print(self.cnt)
            rot_os = self.rot_osi[self.cnt]
            rot_k = self.rot_kut[self.cnt]
            ori = self.cilj_ori[self.cnt]

            glPushMatrix()
            glTranslatef(ori[0] / self.spline.scale, ori[1] / self.spline.scale, ori[2] / self.spline.scale)
            glRotatef(rot_k, rot_os[0], rot_os[1], rot_os[2])
            self.obj.draw()
            glPopMatrix()


B = bspline()

O = object()

cilj_ori = list()
tangente = list()
rot_osi = list()
rot_kut = list()
i = 1
s = 0.05
trenutni = [0, 0, 1]

for tocka in B.tocke:
    t = 0
    while t < 1:
        cilj = B.izracunaj_orijentaciju(i, t)
        cilj_ori.append(cilj)
        tan = B.izracunaj_tangentu(i, t)
        tangente.append(tan)
        rot_kut.append(O.izracunaj_kut(trenutni, tan))
        rot_osi.append(O.izracunaj_os(trenutni, tan))
        t += s
        trenutni = tan + cilj
    i += 1
    if i == len(B.tocke) - 3 + 1:
        break

window = Window(1280, 960, 'sim', cilj_ori, tangente, rot_kut, rot_osi)
pyglet.app.run()
